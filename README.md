# PoC pr0gramm VR App

Test project to show content of pr0gramm in VR.  

Demo see: https://pr0gramm.com/new/4114740

## Features
* Fetch Benis
* Show images (thumbnails + full)
* fetch more images if you are on the last one
* change category (SFW, NSFW, NSFL)
* Show top or new
* Show Benis of image
* Up and Downvote images
* Show comments (very limited)
* Change to next or previous image (A and B button)
* Choose image with pointer

Controls are currently only tested and configured for the right Valve Index controller.

## Prerequisites
* Unity 2020.1.3f1
* Steam VR
* Valve Index + Valve Index Controller *

\* May also work with other devices. But not tested and controller mapping will need to be adapted.

## How to use
1. Open project
1. Add Authentication settings (see below)
1. Press Play button
1. ...
1. Profit

If something does not work, please have a look at the logs. E.g. if a 401 status code is present, the data you provided for login is not correct.

### Authentication
Login is currently not supported (not sure of the API and also a captcha is needed). To authenticate you need to put a cookie from your browser into file `Assets\Resources\application.json` (needs to be created on your own).
```
{
	"authCookie": "TODO"
}
```

The cookie may need to be refreshed from time to time, as it may not be valid anymore after x days.

#### How to get the cookie
1. Open pr0gramm.com in your browser
1. Login
1. Press F12 to open `DevTools`
1. Change to the `Network` tab
1. Press F5 to refresh the page and see the requests made in the tab
1. Select a request like `get?flags=....`
1. From the `Request Headers` of this request copy the value of `cookie`
    * looks like: cookie: `__cfduid=d1345...2%3A1%7D`
1. Set this as `authCookie` in the json file.

## Known issues
* When exporting the app as .exe file, the controller input does not work

## Unknown issues
If you have an issue which is not listed here, feel free to open an issue here.
