﻿[System.Serializable]
public class Item
{
    public int id;
    public int promoted;

    public bool audio;
    public int height;
    public int width;

    public string image;
    public string thumb;

    public int up;
    public int down;
    public int flags;
}
