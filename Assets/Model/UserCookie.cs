﻿[System.Serializable]
public class UserCookie
{
    public int t;
    public int lv;
    public bool paid;
    public int vv;
    public bool vm;
    public string n;
    public string id;
    public int a;
    public string pp;
    public int fl;
}