﻿using System.Collections.Generic;

[System.Serializable]
public class ItemInfo
{
    public string cache;
    public List<Comment> comments;
    public int qc;
    public int rt;
    public List<Tag> tags;
    public int ts;
}
