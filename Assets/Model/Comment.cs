﻿[System.Serializable]
public class Comment
{
    public double confidence;
    public string content;
    public int created;
    public int down;
    public int up;
    public int id;
    public int mark;
    public string name;
    public int parent;
}
