﻿using System.Collections.Generic;

[System.Serializable]
public class RootObject
{
    public bool atEnd;
    public bool atStart;
    public string cache;
    public string error;

    public List<Item> items;

    public int qc;
    public int rt;
    public int ts;

}