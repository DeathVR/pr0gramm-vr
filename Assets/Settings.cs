﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Setting
{
    public string authCookie;
}

public class Settings
{
    public static Settings Default = new Settings("application");

    private Dictionary<string, string> properties = new Dictionary<string, string>();

    private Setting setting;

    private Settings(string filename)
    {
        load(filename);
    }

    private void load(string fileName)
    {
        Debug.Log("Loading settings from " + fileName);
        string json = (Resources.Load(fileName, typeof(TextAsset)) as TextAsset).text;
        setting = JsonUtility.FromJson<Setting>(json);
    }

    public Setting get()
    {
        return this.setting;
    }
}